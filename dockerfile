FROM telegraf:1.8.0

COPY ./telegraf.conf /etc/telegraf/telegraf.conf

ENTRYPOINT ["/entrypoint.sh"]

CMD ["telegraf"]
